import '../scss/app.scss';
import Siema from 'siema';
import Awesomplete from 'awesomplete';

document.addEventListener('DOMContentLoaded', () => {
// hamburger animation
  const hamburger = document.querySelector('#nav-icon');
  hamburger.addEventListener('click', () => {
    hamburger.classList.toggle('open');
  });

  // main slider
  Siema.prototype.addPagination = function () {
    const wrapper = document.createElement('div');
    wrapper.className = 'dotsWrapper';
    this.selector.appendChild(wrapper);
    for (let i = 0; i < this.innerElements.length; i++) {
      const btn = document.createElement('button');
      if (i === 0) {
        btn.className = 'active';
      }
      btn.textContent = i;
      btn.addEventListener('click', () => this.goTo(i));
      wrapper.appendChild(btn);
    }
  };

  function onInitCallback() {
    this.addPagination();
  }

  function onChangeCallback() {
    const dotsArray = this.selector.children[1].children;
    for (let i = 0; i < dotsArray.length; i++) {
      if (i === this.currentSlide) {
        dotsArray[i].classList.add('active');
      } else {
        dotsArray[i].classList.remove('active');
      }
    }
  }

  const mainSlider = new Siema({
    selector: '.slider__wrapper--slides',
    duration: 200,
    easing: 'ease-out',
    perPage: 1,
    startIndex: 0,
    draggable: true,
    multipleDrag: true,
    threshold: 20,
    loop: true,
    onInit: onInitCallback,
    onChange: onChangeCallback,
  });

  const mainSliderPrev = document.querySelector('.mainSliderPrev');
  const mainSliderNext = document.querySelector('.mainSliderNext');

  mainSliderPrev.addEventListener('click', () => mainSlider.prev());
  mainSliderNext.addEventListener('click', () => mainSlider.next());

  // product slider
  const productSlider = new Siema({
    selector: '.product-carousel--wrapper',
    duration: 200,
    easing: 'ease-out',
    perPage: {
      375: 1,
      480: 2,
      600: 3,
      800: 5,
      1140: 6,
    },
    startIndex: 0,
    draggable: true,
    multipleDrag: true,
    threshold: 20,
    loop: true,
  });

  const productSliderPrev = document.querySelector('.productSliderPrev');
  const productSliderNext = document.querySelector('.productSliderNext');

  productSliderPrev.addEventListener('click', () => productSlider.prev());
  productSliderNext.addEventListener('click', () => productSlider.next());

  // popup
  const addToBasketButtonsArray = document.querySelectorAll('.product--info > button');
  const popup = document.querySelector('#popup');
  const popupClose = document.querySelector('.popup-close');
  const popupContent = document.querySelector('#popup .content');

  addToBasketButtonsArray.forEach((item) => {
    item.addEventListener('click', () => {
      popup.classList.add('active');
      popupContent.innerHTML = `<h2>DODAŁEŚ DO KOSZYKA</h2><br>${item.previousSibling.previousSibling.innerText}`;
    });
  });

  popupClose.addEventListener('click', () => {
    popup.classList.remove('active');
  });

  // live search
  async function loadJSON(url) {
    const res = await fetch(url);
    return await res.json();
  }

  loadJSON('./data.json').then((data) => {

  });

  const searchContainer = document.querySelector('#searchInput');
  const awesomplete = new Awesomplete(searchContainer);

  searchContainer.addEventListener('keyup', () => {
    loadJSON('./data.json').then((data) => {
      awesomplete.list = data.map(item => item.name);
    });
  });

  // simple countdown
  const countdownToDate = setInterval(function () {
    const dateFuture = new Date(new Date().getFullYear() + 1, 0, 1);
    const dateNow = new Date();

    let seconds = Math.floor((dateFuture - (dateNow)) / 1000);
    let minutes = Math.floor(seconds / 60);
    let hours = Math.floor(minutes / 60);
    let days = Math.floor(hours / 24);

    hours -= (days * 24);
    minutes = minutes - (days * 24 * 60) - (hours * 60);
    seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);

    document.querySelector('.featured--info-counter--box').innerHTML = `<span>${days}</span> dni <span>${hours}</span> : <span>${minutes}</span>: <span>${seconds}</span>`;
  }, 1000);
});
