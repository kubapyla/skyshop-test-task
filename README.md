# Sky-Shop.pl test task

* Configure `webpack.config.js`
    * Define at the beginning of the file the server path to your project location.

```js
const localServer = {
  path: 'localhost/',
  port: 3000
};
```

### Assets Source

* _SASS_ files are located under `/src/scss/`
* _JavaScript_ files with support of _ES6_ files are located under `/src/js/`
* _Images_ are located in `/src/images/`
* _Fonts_ are located in `/src/fonts/`
* _HTML_ files are located in `/src/` 

### Run Code Style Linters

* _SASS_ - `yarn run lint-sass`
* _JS_ - `yarn run lint-js`

### Build Assets

* Execute `yarn install`
* Execute `yarn run build`
* Enable source files watching `yarn run watch`
* Optimize assets for production with `yarn run production`

### Processed Assets

* _CSS_ files are located under `/dist/css/`
* _JavaScript_ files with support of _ES6_ files are located under `/dist/js/`
* _Images_ are located in `/dist/images/`
* _Fonts_ are located in `/dist/fonts/`
* _HTML_ files are located in `/dist/`
